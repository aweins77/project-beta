import django
import os
import sys
import time
import json
import requests

sys.path.append("")
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "sales_project.settings")
django.setup()

# Import models from sales_rest, here.
# from sales_rest.models import Something
from sales_rest.models import AutomobileVO

def get_Automobiles():
    url = "http://inventory-api:8000/api/automobiles/"
    response = requests.get(url)
    content = json.loads(response.content)
    print(len(content["autos"]))
    # print(content)
    for auto in content["autos"]:
        print(auto["vin"])
        # print(auto["color"])
        AutomobileVO.objects.update_or_create(
            vin=auto["vin"],
            defaults={
                "vin": auto["vin"],
                "sold": auto["sold"],
            }
        )


def poll(repeat=True):
    while True:
        print('Sales poller polling for data')
        try:
            get_Automobiles()

        except Exception as e:
            print(e, file=sys.stderr)

        if (not repeat):
            break

        time.sleep(10)


if __name__ == "__main__":
    poll()
