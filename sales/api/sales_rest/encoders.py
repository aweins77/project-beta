from common.json import ModelEncoder

from .models import AutomobileVO,Customer,Sale,Salesperson

class AutomobileVOEncoder(ModelEncoder):
    model = AutomobileVO
    properties = [
        "vin",
        "sold",
    ]

class CustomerEncoder(ModelEncoder):
    model=Customer
    properties=[
        "first_name",
        "last_name",
        "address",
        "phone_number",
        "id",
    ]

class SalesPersonEncoder(ModelEncoder):
    model=Salesperson
    properties=[
        "first_name",
        "last_name",
        "employee_id",
        "id",
    ]

class SaleEncoder(ModelEncoder):
    model=Sale
    properties=[
        "automobile",
        "salesperson",
        "customer",
        "price",
        "id",

    ]
    encoders = {
        "automobile":AutomobileVOEncoder(),
        "salesperson":SalesPersonEncoder(),
        "customer":CustomerEncoder(),
    }
