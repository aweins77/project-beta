from django.urls import path
from .views import customers_api,delete_customer_api,salesperson_api,sale_api


urlpatterns = [
   path("customers/", customers_api, name="customers_api"),
   path("customers/<int:id>",delete_customer_api, name="delete_customer_api"),
   path("salespeople/", salesperson_api, name="salesperson_api"),
   path("sales/", sale_api, name="salesperson_api"),

]
