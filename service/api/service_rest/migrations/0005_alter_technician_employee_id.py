# Generated by Django 4.0.3 on 2023-06-07 00:23

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('service_rest', '0004_status_remove_appointment_canceled_and_more'),
    ]

    operations = [
        migrations.AlterField(
            model_name='technician',
            name='employee_id',
            field=models.AutoField(primary_key=True, serialize=False),
        ),
    ]
