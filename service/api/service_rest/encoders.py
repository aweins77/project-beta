from common.json import ModelEncoder

from .models import AutomobileVO, Technician, Appointment


class AutomobileVOEncoder(ModelEncoder):
    model = AutomobileVO
    properties = [
        "vin",
        "sold",
    ]


class TechnicianEncoder(ModelEncoder):
    model = Technician
    properties = [
        "first_name",
        "last_name",
        "employee_id"
    ]


class AppointmentEncoder(ModelEncoder):
    model = Appointment
    properties = [
        'id',
        'vin',
        'customer',
        'is_vip',
        'date_time',
        'technician',
        'reason',
        'status',
    ]

    encoders = {
        "technician": TechnicianEncoder(),
    }
