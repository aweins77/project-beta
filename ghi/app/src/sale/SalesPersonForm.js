import React, { useState } from "react";

function SalesPersonForm({getSalesPersons}){
    const[first_name,setFirstName]=useState('');
    const[last_name,setLastName]=useState('');
    const[employee_id,setEmployeeID]=useState('');



    function handleChangeFirstName(event) {
        const value = event.target.value;
        setFirstName(value);
    }
    function handleChangeLastName(event) {
        const value = event.target.value;
        setLastName(value);
    }
    function handleChangeEmployeeID(event) {
        const value = event.target.value;
        setEmployeeID(value);
    }

    async function handleSubmit(event) {
        event.preventDefault();
        const data = {
            first_name,
            last_name,
            employee_id,
        };

        const SalesPersonUrl = 'http://localhost:8090/api/salespeople/'
        const fetchConfig = {
            method: 'POST',
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json'
            }
        };

        const response = await fetch(SalesPersonUrl, fetchConfig);
        if (response.ok) {
            const newSalesPerson = await response.json();
            console.log(newSalesPerson);
            getSalesPersons();

            setFirstName('');
            setLastName('');
            setEmployeeID('');

        };
    }
    return (
        <div className="row">
          <div className="offset-3 col-6">
            <div className="shadow p-4 mt-4">
              <h1>Add a SalesPerson!</h1>
              <form onSubmit={handleSubmit} id="create-technician-form">
                <div className="form-floating mb-3">
                  <input onChange={handleChangeFirstName} value={first_name} placeholder="First Name" required type="text" name="first name" id="first name" className="form-control" />
                  <label htmlFor="name">First name</label>
                </div>
                <div className="form-floating mb-3">
                  <input onChange={handleChangeLastName} value={last_name} placeholder="Last Name" required type="text" name="last name" id="last name" className="form-control" />
                  <label htmlFor="name">Last name</label>
                </div>
                <div className="form-floating mb-3">
                  <input onChange={handleChangeEmployeeID} value={employee_id} placeholder="Employee_id" required type="text" name="employee_id" id="employee_id" className="form-control" />
                  <label htmlFor="name">Employee ID</label>
                </div>
                <button className="btn btn-primary">Create a SalesPerson</button>
              </form>
            </div>
          </div>
        </div>
      );
    }

    export default SalesPersonForm;
