

import React, {useState,useEffect} from "react";




function SalesHistory(){
const[salespersons, setSalesPersons]= useState([]);
const[sales,setSales]=useState([]);
const[selectedsalesperson,setSelectedSalesperson]=useState('');


    async function getSalesPersons() {
        const url = 'http://localhost:8090/api/salespeople/';
        const response = await fetch(url);
        if (response.ok) {
          const data = await response.json();
          setSalesPersons(data.salespersons)
          console.log("salespersons", data.salespersons)
        } else {
          console.error(response);
        }
      }
      async function getSales() {
        const url = 'http://localhost:8090/api/sales/';
        const response = await fetch(url);
        if (response.ok) {
          const data = await response.json();
          setSales(data.sales)
          console.log("sales", data.sales)
        } else {
          console.error(response);
        }
      }


useEffect(() => {
  getSalesPersons();
  getSales();

  }, []);

  function handleSelectedSalesPersonChange(event) {
    const value = event.target.value;
    setSelectedSalesperson(value);


}


return (
    <div className="row">
    <div className="offset-3 col-6">
      <div className="shadow p-4 mt-4">
        <h1>List Sales History</h1>
        <div className="mb-3">
            <select  onChange={handleSelectedSalesPersonChange
            }id="salespersons" name="salespersons" className="form-select">
              <option value={selectedsalesperson}>Choose a Salesperson</option>
              {salespersons.map(salesperson => {
                 return (
                 <option  key={salesperson.id} value={salesperson.id}>
                     {salesperson.first_name} {salesperson.last_name}
                 </option>

               );
             })}
             </select>
          </div>
          </div>
          </div>
         <table className="table table-striped">
          <thead>
            <tr>
              <th>Employee ID</th>
              <th>Salesperson Name</th>
              <th>Customer</th>
              <th>VIN</th>
              <th>Price</th>
            </tr>
          </thead>
          <tbody>

            {sales.filter(sale => sale.salesperson.id.toString() === selectedsalesperson).map( sale =>{
              return (
                <tr key={sale.id}>
                  <td>{ sale.salesperson.employee_id}</td>
                  <td>{sale.salesperson.first_name } {sale.salesperson.last_name }</td>
                  <td>{ sale.customer.first_name} { sale.customer.last_name}</td>
                  <td>{ sale.automobile.vin}</td>
                  <td>{ sale.price }</td>
                </tr>
              );
            })}
          </tbody>
        </table>
       </div>




  )
}















export default SalesHistory
