import React, { useState } from "react";

function CustomerForm({getCustomers}){
    const[first_name,setFirstName]=useState('');
    const[last_name,setLastName]=useState('');
    const[address,setAddress]=useState('');
    const[phone_number,setPhoneNumber]=useState('');


    function handleChangeFirstName(event) {
        const value = event.target.value;
        setFirstName(value);
    }
    function handleChangeLastName(event) {
        const value = event.target.value;
        setLastName(value);
    }
    function handleChangeAddress(event) {
        const value = event.target.value;
        setAddress(value);
    }
    function handleChangePhoneNumber(event) {
        const value = event.target.value;
        setPhoneNumber(value);
    }
    async function handleSubmit(event) {
        event.preventDefault();
        const data = {
            first_name,
            last_name,
            address,
            phone_number,
        };

        const customerUrl = 'http://localhost:8090/api/customers/'
        const fetchConfig = {
            method: 'POST',
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json'
            }
        };

        const response = await fetch(customerUrl, fetchConfig);
        if (response.ok) {
            const newCustomer = await response.json();
            console.log(newCustomer);
            getCustomers();

            setFirstName('');
            setLastName('');
            setAddress('');
            setPhoneNumber('');

        };
    }
    return (
        <div className="row">
          <div className="offset-3 col-6">
            <div className="shadow p-4 mt-4">
              <h1>Add a Customer!</h1>
              <form onSubmit={handleSubmit} id="create-technician-form">
                <div className="form-floating mb-3">
                  <input onChange={handleChangeFirstName} value={first_name} placeholder="First Name" required type="text" name="first name" id="first name" className="form-control" />
                  <label htmlFor="name">First name</label>
                </div>
                <div className="form-floating mb-3">
                  <input onChange={handleChangeLastName} value={last_name} placeholder="Last Name" required type="text" name="last name" id="last name" className="form-control" />
                  <label htmlFor="name">Last name</label>
                </div>
                <div className="form-floating mb-3">
                  <input onChange={handleChangeAddress} value={address} placeholder="Address" required type="text" name="address" id="address" className="form-control" />
                  <label htmlFor="name">Address</label>
                </div>
                <div className="form-floating mb-3">
                  <input onChange={handleChangePhoneNumber} value={phone_number} placeholder="Phone Number" required type="text" name="phone number" id="phone number" className="form-control" />
                  <label htmlFor="name">Phone Number</label>
                </div>
                <button className="btn btn-primary">Create a customer!</button>
              </form>
            </div>
          </div>
        </div>
      );
    }

    export default CustomerForm;
