import React, {useEffect, useState} from "react";

function CreateSaleForm({getSales}){
    const[salespersons, setSalesPersons]= useState([]);
    const [automobiles, setAutomobiles ] = useState([]);
    const[ customers , setCustomers ] = useState([]);


    const[selectedsalesperson,setSelectedSalesperson]=useState('');
    const[selectedautomobile, setSelectedAutomobile]=useState('');
    const [selectedcustomer , setSelectedCustomer ] = useState('');
    const[price, setPrice]=useState('');


    async function getSalesPersons() {
        const url = 'http://localhost:8090/api/salespeople/';
        const response = await fetch(url);
        if (response.ok) {
          const data = await response.json();
          setSalesPersons(data.salespersons)
          console.log("salespersons", data.salespersons)
        } else {
          console.error(response);
        }
      }

      async function getAutomobiles() {
        const url= 'http://localhost:8100/api/automobiles/';
        const response = await fetch(url);
        if(response.ok){
          const data = await response.json();
          setAutomobiles(data.autos);

        } else {
          console.error(response)
        }
      }
      async function getCustomers() {
        const url = 'http://localhost:8090/api/customers/';
        const response = await fetch(url);
        if (response.ok) {
          const data = await response.json();
          setCustomers(data.customers)

        } else {
          console.error(response);
        }
      }
      useEffect(() => {
        getSalesPersons();
        getCustomers();
        getAutomobiles();
        getSales();
        }, []);
    function handleSelectedAutomobileChange(event) {
            const value = event.target.value;
            setSelectedAutomobile(value);
        }
    function handleSelectedSalesPersonChange(event) {
            const value = event.target.value;
            setSelectedSalesperson(value);
        }
    function handleSelectedCustomerChange(event) {
            const value = event.target.value;
            setSelectedCustomer(value);
        }
        function handlePriceChange(event) {
            const value = event.target.value;
            setPrice(value);
        }

  async function handleSubmit(event) {
  event.preventDefault();

  const data = {
    automobile: selectedautomobile,
    salesperson: selectedsalesperson,
    customer: selectedcustomer,
    price: price,
}

  const salesUrl = 'http://localhost:8090/api/sales/';
  const fetchConfig = {
            method: 'POST',
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json'
            }
        };
   console.log(fetchConfig)
   const response = await fetch(salesUrl, fetchConfig);
        if (response.ok) {
            const newSale = await response.json();
            console.log(newSale);
            getSales();


           setSalesPersons([]);
           setAutomobiles([]);
           setCustomers([]);
              setPrice('');

        };
    }


return(

        <div className="row">
          <div className="offset-3 col-6">
            <div className="shadow p-4 mt-4">
              <h1>Record a Sale</h1>
               <form onSubmit={handleSubmit } id="create-sales-form">
                <div className="mb-3">

              <select  onChange={handleSelectedAutomobileChange}id="vins" name="vins" className="form-select">
              <option value={selectedautomobile}>Choose a VIN</option>
              {automobiles.map(auto => {
                 return (
                 <option  key={auto.id} value={auto.vin}>
                     {auto.vin}
                 </option>
                 );
             })}
             </select>
          </div>
          <div className="mb-3">
          <select  onChange={handleSelectedSalesPersonChange} id="salespersons" name="salespersons" className="form-select">
              <option value={selectedsalesperson}>Choose a Salesperson</option>
              {salespersons.map(salesperson => {
                 return (
                 <option  key={salesperson.id} value={salesperson.id}>
                     {salesperson.first_name} {salesperson.last_name}
                 </option>
              );
             })}
             </select>
            </div>
          <div className="mb-3">
          <select  onChange={handleSelectedCustomerChange }id="customers" name="customers" className="form-select">
              <option value={selectedcustomer}>Choose a Customer</option>
              {customers.map(customer => {
                 return (
                 <option  key={customer.id} value={customer.id}>
                     {customer.first_name} {customer.last_name}
                 </option>
               );
             })}
              </select>
                </div>
             <div className="form-floating mb-3">
                  <input onChange={handlePriceChange} value={price} placeholder="Price" required type="text" name="price" id="price" className="form-control" />
                  <label htmlFor="name">Price</label>
                </div>
                <button className="btn btn-primary">Create a Sales Record</button>
                </form>
                </div>
                </div>
            </div>
















    )
}

export default CreateSaleForm
