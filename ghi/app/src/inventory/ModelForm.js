import {useState} from "react"


function ModelForm({ manufacturers, getModels }){
    const[ name,SetName ] = useState('');
    const[ picture_url,SetPicture_Url ] = useState('');
    const[ manufacturer_id,SetManufacturer_Id ] = useState('');

async function handleSubmit(event) {
  event.preventDefault();
  const data = {
    name,
    picture_url,
    manufacturer_id,
  }

  const modelUrl = 'http://localhost:8100/api/models/'
  const fetchConfig = {
    method: 'POST',
    body: JSON.stringify(data),
    headers: {
      'Content-Type': 'application/json'
    }
  };

  const response = await fetch(modelUrl, fetchConfig);
  if (response.ok) {
    getModels();

    SetName('');
    SetPicture_Url('');
    SetManufacturer_Id('');
  }
}

function handleChangeName(event) {
    const value = event.target.value;
    SetName(value);
}

function handleChangePicture_Url(event) {
  const value = event.target.value;
  SetPicture_Url(value);
}

function handleChangeManufacturer_Id(event) {
  const value = event.target.value;
  SetManufacturer_Id(value);
}


return(
        <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Create a vehicule model</h1>
            <form  onSubmit={handleSubmit} id="create-model-form">
              <div className="form-floating mb-3">
                <input onChange={handleChangeName} value={name} placeholder="Model" required type="text" name="model" id="model" className="form-control"/>
                <label htmlFor="model">Model name...</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={handleChangePicture_Url} value={picture_url} placeholder="Picture_url" required type="text" name="picture_url" id="picture_url" className="form-control"/>
                <label htmlFor="picture_url">Picture URL...</label>
              </div>
              <div className="mb-3">
                <select onChange={handleChangeManufacturer_Id} value={manufacturer_id} required name="manufacturer_id" id="manufacturer_id" className="form-select">
                  <option  value="">Choose a Manufacturer...</option>
                  {manufacturers.map(manufacturer => {
                     return (
                     <option  key={manufacturer.id} value={manufacturer.id}>
                         {manufacturer.name}
                     </option>
                     );
                 })}
                 </select>
              </div>
              <button className="btn btn-primary">Create</button>
            </form>
          </div>
        </div>
      </div>
        );
}

export default ModelForm;
