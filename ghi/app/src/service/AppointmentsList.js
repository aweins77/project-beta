import React, { useState } from 'react';
import { useNavigate, NavLink } from "react-router-dom";

function AppointmentList({ appointments, getAppointments }) {
  const navigate = useNavigate();
  const [searchQuery, setSearchQuery] = useState('');
  const [filteredAppointments, setFilteredAppointments] = useState([]);

  const cancelAppointment = async (id) => {
    try {
      const response = await fetch(`http://localhost:8080/api/appointments/${id}/cancel/`, {
        method: "PUT",
      });
      if (response.ok) {
        navigate('/appointments');
        getAppointments();
      }
    } catch (error) {
      console.error('Error occurred while finishing appointment', error);
    }
  }

  const finishAppointment = async (id) => {
    try {
      const response = await fetch(`http://localhost:8080/api/appointments/${id}/finish/`, {
        method: "PUT",
      });
      if (response.ok) {
        navigate('/appointments');
        getAppointments();
      }
    } catch (error) {
      console.error('Error occurred while finishing appointment', error);
    }
  }

  const handleSearchInputChange = (event) => {
    const { value } = event.target;
    setSearchQuery(value);

    const filtered = appointments.filter(appointment => appointment.vin.toLowerCase().includes(value.toLowerCase()));
    setFilteredAppointments(filtered);
  }

  const bookedAppointments = appointments.filter(appointment => appointment.status === 'BOOKED');
  const updatedAppointments = filteredAppointments.length > 0 ? filteredAppointments : appointments.filter(appointment => appointment.status !== 'BOOKED');

  if (appointments === undefined) {
    return null;
  }

  return (
    <>
      <div>
        <h1 className="font-weight-bold mt-4"> Service Appointments </h1>
        <table className="table table-striped">
          <thead>
            <tr>
              <th>VIN</th>
              <th>Customer</th>
              <th>Is VIP?</th>
              <th>Date</th>
              <th>Time</th>
              <th>Technician</th>
              <th>Reason</th>
              <th></th>
            </tr>
          </thead>
          <tbody>
            {bookedAppointments.map(bookedAppointment => {
              const [date, time] = bookedAppointment.date_time.split('T');
              const bookedAppointmentDate = new Date(date);
              const formattedDate = bookedAppointmentDate.toLocaleDateString('en-US', { day: '2-digit', month: '2-digit', year: 'numeric' });
              const bookedAppointmentTime = new Date(`1970-01-01T${time}`);
              const formattedTime = bookedAppointmentTime.toLocaleTimeString('en-US', { hour: '2-digit', minute: '2-digit', hour12: true });
              const isVip = bookedAppointment.is_vip ? "Yes" : "No";
              return (
                <tr key={bookedAppointment.id}>
                  <td>{bookedAppointment.vin}</td>
                  <td>{bookedAppointment.customer}</td>
                  <td>{isVip}</td>
                  <td>{formattedDate}</td>
                  <td>{formattedTime}</td>
                  <td>{`${bookedAppointment.technician.last_name} ${bookedAppointment.technician.first_name}`}</td>
                  <td>{bookedAppointment.reason}</td>
                  <td>
                    <button
                      onClick={() => {
                        cancelAppointment(bookedAppointment.id);
                        alert('Appointment canceled successfully');
                      }}
                      className="btn btn-danger mx-3"
                    >
                      Cancel
                    </button>
                    <button
                      onClick={() => {
                        finishAppointment(bookedAppointment.id);
                        alert('Appointment finished successfully');
                      }}
                      className="btn btn-success"
                    >
                      Finish
                    </button>
                  </td>
                </tr>
              );
            })}
          </tbody>
        </table>
      </div>
      <div>
        <h1 className="font-weight-bold mt-4"> Service History </h1>
        <div className="mb-3">
          <input
            type="text"
            className="form-control"
            placeholder="Search Service History by VIN"
            value={searchQuery}
            onChange={handleSearchInputChange}
          />
        </div>
        <table className="table table-striped">
          <thead>
            <tr>
              <th>VIN</th>
              <th>Customer</th>
              <th>Is VIP?</th>
              <th>Date</th>
              <th>Time</th>
              <th>Technician</th>
              <th>Reason</th>
              <th>Status</th>
            </tr>
          </thead>
          <tbody>
            {updatedAppointments.map(updatedAppointment => {
              const [date, time] = updatedAppointment.date_time.split('T');
              const updatedAppointmentDate = new Date(date);
              const formattedDate = updatedAppointmentDate.toLocaleDateString('en-US', { day: '2-digit', month: '2-digit', year: 'numeric' });
              const updatedAppointmentTime = new Date(`1970-01-01T${time}`);
              const formattedTime = updatedAppointmentTime.toLocaleTimeString('en-US', { hour: '2-digit', minute: '2-digit', hour12: true });
              const isVip = updatedAppointment.is_vip ? "Yes" : "No";
              return (
                <tr key={updatedAppointment.id}>
                  <td>{updatedAppointment.vin}</td>
                  <td>{updatedAppointment.customer}</td>
                  <td>{isVip}</td>
                  <td>{formattedDate}</td>
                  <td>{formattedTime}</td>
                  <td>{`${updatedAppointment.technician.last_name} ${updatedAppointment.technician.first_name}`}</td>
                  <td>{updatedAppointment.reason}</td>
                  <td>{updatedAppointment.status}</td>
                </tr>
              );
            })}
          </tbody>
        </table>
        <div className="mb-3">
          <NavLink to="/appointments/new" className="btn btn-outline-primary mb-3">Create a service appointment</NavLink>
        </div>
      </div>
    </>
  );
}

export default AppointmentList;
