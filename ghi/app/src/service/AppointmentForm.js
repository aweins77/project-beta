import React, { useState } from "react";


function AppointmentForm({ getAppointments, technicians }) {
    const [ vin, setVin ] = useState('')
    const [ customer, setCustomer ] = useState('')
    const [ date, setDate ] = useState('')
    const [ time, setTime ] = useState('')
    const [ technician, setTechnician ] = useState('')
    const [ reason, setReason ] = useState('')

async function handleSubmit(event) {
    event.preventDefault();
    const dateTime = `${date}T${time}`;
    const data = {
        vin,
        customer,
        date_time: dateTime,
        technician,
        reason
    };

    const appointmentUrl = 'http://localhost:8080/api/appointments/'
    const fetchConfig = {
        method: 'POST',
        body: JSON.stringify(data),
        headers: {
            'Content-Type': 'application/json'
        }
    };

    const response = await fetch(appointmentUrl, fetchConfig);
    if (response.ok) {
        getAppointments();

        setVin('');
        setCustomer('');
        setDate('');
        setTime('');
        setTechnician('');
        setReason('');
    };
}

function handleChangeVin(event) {
    const value = event.target.value;
    setVin(value);
}

function handleChangeCustomer(event) {
    const value = event.target.value;
    setCustomer(value);
}

function handleChangeDate(event) {
    const value = event.target.value;
    setDate(value);
}

function handleChangeTime(event) {
    const value = event.target.value;
    setTime(value);
}

function handleChangeTechnician(event) {
    const value = event.target.value;
    setTechnician(value);
}
function handleChangeReason(event) {
    const value = event.target.value;
    setReason(value);
}


return (
    <div className="row">
      <div className="offset-3 col-6">
        <div className="shadow p-4 mt-4">
          <h1>Create a service appointment</h1>
          <form onSubmit={handleSubmit} id="create-technician-form">
            <div className="form-floating mb-3">
              <input onChange={handleChangeVin} value={vin} placeholder="Vin" required type="text" name="vin" id="vin" className="form-control" />
              <label htmlFor="name">Automobile VIN...</label>
            </div>
            <div className="form-floating mb-3">
              <input onChange={handleChangeCustomer} value={customer} placeholder="Customer" required type="text" name="customer" id="customer" className="form-control" />
              <label htmlFor="name">Customer...</label>
            </div>
            <div className="form-floating mb-3">
              <input onChange={handleChangeDate} value={date} placeholder="Date" required type="date" name="date" id="date" className="form-control" />
              <label htmlFor="date">Date...</label>
            </div>
            <div className="form-floating mb-3">
              <input onChange={handleChangeTime} value={time} placeholder="Time" required type="time" name="time" id="time" className="form-control" />
              <label htmlFor="time">Time...</label>
            </div>
            <div className="form-floating mb-3">
              <select onChange={handleChangeTechnician} value={technician} placeholder="Technician" required type="text" name="technician" id="technician" className="form-select">
                <option value="">Choose a technician...</option>
                {technicians.map(technician => {
                    return (
                        <option key={technician.employee_id} value={technician.employee_id}>
                            {technician.last_name} {technician.first_name}
                        </option>
                    )
                })}
              </select>
            </div>
            <div className="form-floating mb-3">
              <input onChange={handleChangeReason} value={reason} placeholder="Reason" required type="text" name="reason" id="reason" className="form-control" />
              <label htmlFor="name">Reason...</label>
            </div>
            <button className="btn btn-primary">Create</button>
          </form>
        </div>
      </div>
    </div>
  );
}

export default AppointmentForm;
