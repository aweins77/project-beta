import React, { useState } from "react";


function TechniciansForm({ getTechnicians }) {
    const [ first_name, setFirstName ] = useState('')
    const [ last_name, setLastName ] = useState('')

async function handleSubmit(event) {
    event.preventDefault();
    const data = {
        first_name,
        last_name
    };

    const technicianUrl = 'http://localhost:8080/api/technicians/'
    const fetchConfig = {
        method: 'POST',
        body: JSON.stringify(data),
        headers: {
            'Content-Type': 'application/json'
        }
    };

    const response = await fetch(technicianUrl, fetchConfig);
    if (response.ok) {
        getTechnicians();

        setFirstName('');
        setLastName('');
    };
}

function handleChangeFirstName(event) {
    const value = event.target.value;
    setFirstName(value);
}

function handleChangeLastName(event) {
    const value = event.target.value;
    setLastName(value);
}


return (
    <div className="row">
      <div className="offset-3 col-6">
        <div className="shadow p-4 mt-4">
          <h1>Add a Technician</h1>
          <form onSubmit={handleSubmit} id="create-technician-form">
            <div className="form-floating mb-3">
              <input onChange={handleChangeFirstName} value={first_name} placeholder="First Name" required type="text" name="first name" id="first name" className="form-control" />
              <label htmlFor="name">First name...</label>
            </div>
            <div className="form-floating mb-3">
              <input onChange={handleChangeLastName} value={last_name} placeholder="Last Name" required type="text" name="last name" id="last name" className="form-control" />
              <label htmlFor="name">Last name...</label>
            </div>
            <button className="btn btn-primary">Create</button>
          </form>
        </div>
      </div>
    </div>
  );
}

export default TechniciansForm;
